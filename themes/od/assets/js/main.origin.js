if (typeof replaceAll !== 'function') {
    String.prototype.replaceAll = function(search, replacement) {
        var target = this;
        return target.replace(new RegExp(search, 'g'), replacement);
    };
}

/**
 * Global variables and functions
 */
var Project = (function($, window, undefined) {
    try {
        var $win = $(window),
            $html = $('html'),
            $btnToggleNav = $('.header').find('.btn-toggle-nav'),
            $btnToggleNavRight = $('.header').find('.btn-toggle-nav-right'),
            $switchControl = $('.switch-control'),
            $btnClose = $('.timeline-block').find('.btn-close'),
            $btnTreeCheckboxs = $('.tree-checkbox').find('.btn-arrow'),
            $newVeld = $('.ontwerp-componnent').find('.new-veld'),
            $customSelect = $('select.form-control'),
            $dateControl = $('.date-control'),
            $imgZoom = $('.zoom-img');

        _toggleNav = function(e) {
            $html.toggleClass('navigation-lg');
            if ($('#wrap-notification').attr('aria-expanded') == 'true')
                $('#wrap-notification').removeClass('in').attr('aria-expanded', false);
        };

        // developer-1: #771, right-menu
        _toggleNavRight = function(e) {
            $html.toggleClass('navigation-lg-right');
            if ($('#wrap-notification').attr('aria-expanded') == 'true')
                $('#wrap-notification').removeClass('in').attr('aria-expanded', false);
        };

        _initTooltips = function() {
            try {
                var options = {
                    container: '#wrapper',
                    trigger: "hover"
                };
                jQuery('[data-toggle="tooltip"]').tooltip(options);
            } catch (err) {
                return true;
            }
        };

        _initCollapseSwitchControl = function() {
            if ($switchControl.length) {
                $switchControl.each(function(index, el) {
                    var target = $(el).data('target'),
                        $checkbox = $(el).find('input[type="checkbox"]');

                    if (target && $checkbox.prop('checked')) {
                        $(target).addClass('in');
                    } else {
                        $(target).removeClass('in');
                    }
                });
            }
        };

        _closeTimelineBlock = function() {
            $('.timeline-block').addClass('hidden');
        };

        _initSort = function() {
            if ($(".sort-able").length) {
                $(".sort-able").sortable({
                    placeholder: "ui-state-highlight",
                    items: '.block-item'
                });
                $(".sort-able").disableSelection();
            }
        };

        _initMasonryNotification = function() {
            $('#wrap-notification').on('shown.bs.collapse', function() {
                $('.grid-masonry').masonry({
                    itemSelector: '.grid-item'
                });
            });
        };

        _toggleTreeCheckbox = function(e) {
            var $el = $(e.currentTarget);
            $parent = $el.closest('.has-child');

            if (!$parent.hasClass('opened')) {
                $parent.addClass('opened');
                $el.siblings('.sub-tree').slideDown('fast');
            } else {
                $parent.removeClass('opened');
                $el.siblings('.sub-tree').slideUp('fast');
            }
        };

        _stickyElement = function($el) {
            if ($el.length) {
                var offsetTop = $el.offset().top,
                    widthElement = $el.width();

                $el.affix({
                    offset: {
                        top: offsetTop
                    }
                });

                $el.width(widthElement).css('top', 0);
            }
        };

        _destroySticky = function($el) {
            $el.removeClass('affix-top').width('auto');
        };

        _initStickyElement = function() {
            if ($win.width() > 992) {
                _stickyElement($newVeld);
            } else {
                _destroySticky($newVeld);
            }
        };

        _initSelect2 = function() {
            if (!jQuery('#formbuilder_script').length) {
                if ($customSelect.length) {
                    $customSelect.each(function(index, el) {
                        if ($(el).hasClass('custom-select')) {
                            $(el).select2({
                                minimumResultsForSearch: Infinity,
                                width: '100%'
                            });
                        } else {
                            $(el).select2({
                                width: '100%'
                            });
                        }
                    });
                }
            }
        };

        _bindEventToggleNave = function() {
            var timeout;
            $btnToggleNav.on('click.toggleNav', _toggleNav);
            $btnToggleNavRight.on('click.toggleNav', _toggleNavRight);
            $switchControl.on('click.toggleCollapseSwitch', _initCollapseSwitchControl);
            $btnClose.on('click.closeTimeLineBlock', _closeTimelineBlock);
            $btnTreeCheckboxs.on('click.toggleSubTree', _toggleTreeCheckbox);
            $win.on('resize', function() {
                clearTimeout(timeout);
                timeout = setTimeout(function() {
                    _initStickyElement();
                }, 200);
            });
        };

        _bindEvent2CloseNav = function(event) {
            if (!$(event.target).closest('.sidebar').length &&
                !$(event.target).is('.sidebar') &&
                !$(event.target).closest('#wrap-notification').length &&
                !$(event.target).is('#wrap-notification') &&
                !$(event.target).closest('.btn-toggle-nav').length &&
                !$(event.target).is('.btn-toggle-nav') &&
                !$(event.target).closest('.btn-toggle-nav-right').length &&
                !$(event.target).is('.btn-toggle-nav-right')
            ) {
                if ($('html').hasClass('navigation-lg')) {
                    $('html').removeClass('navigation-lg');
                }
                if ($('html').hasClass('navigation-lg-right')) {
                    $('html').removeClass('navigation-lg-right');
                }
                if ($('#wrap-notification').hasClass('in')) {
                    $('#wrap-notification').removeClass('in');
                }
            }
        };

        _touchHandler = function(event) {
            var touches = event.changedTouches,
            first = touches[0],
            type = "";

            // alert('_touchHandler ' + event.type);

            if (event.type == 'touchend') {
                _bindEvent2CloseNav(event);
                return;
            }

            // switch(event.type) {
            //     case "touchstart": type = "mousedown"; break;
            //     case "touchmove":  type = "mousemove"; break;        
            //     case "touchend":   type = "mouseup"; break;
            //     default: return;
            // }

            // //initMouseEvent(type, canBubble, cancelable, view, clickCount, 
            // //           screenX, screenY, clientX, clientY, ctrlKey, 
            // //           altKey, shiftKey, metaKey, button, relatedTarget);

            // var simulatedEvent = document.createEvent("MouseEvent");
            // simulatedEvent.initMouseEvent(type, true, true, window, 1, 
            //     first.screenX, first.screenY, 
            //     first.clientX, first.clientY, false, 
            //     false, false, false, 0/*left*/, null);

            // first.target.dispatchEvent(simulatedEvent);
            // event.preventDefault();
        }

        _initMouseEvents = function () {
            document.addEventListener("touchstart", _touchHandler, true);
            document.addEventListener("touchmove", _touchHandler, true);
            document.addEventListener("touchend", _touchHandler, true);
            document.addEventListener("touchcancel", _touchHandler, true);    
        }; _initMouseEvents();

        $(document).on('click', function(event) {
            // alert('document clicked');
            _bindEvent2CloseNav(event);
        });

        // developer-1: check for more top-right icons
        if ($('#menuIcon-settings').length) {
            $('#wrap-notification').toggleClass('changed');
        }

        _initDateControl = function() {
            if (typeof(Modernizr) === typeof undefined || !Modernizr.inputtypes.date) {
                if ($(':input[type="date"]').length) {
                    $('input[type=date]').each(function() {
                        if (jQuery(this).val()) {
                            var val = jQuery(this).val().split('-');
                            jQuery(this).val(val[2] + '/' + val[1] + '/' + val[0]);
                        }
                    });
                    $('input[type=date]')
                        .attr('type', 'text')
                        .pickadate({
                            format: 'dd/mm/yyyy',
                            formatSubmit: 'yyyy-mm-dd',
                            selectMonths: true,
                            selectYears: true,
                            selectYears: 200,
                            hiddenName: true,
                            hiddenPrefix: '',
                            hiddenSuffix: ''
                        });
                }
            }

            // developer-1: #613 extend 1 more class to apply pickadate
            if (typeof(pickadate) !== typeof undefined) {
                $('input.date-picker')
                    .attr('type', 'text')
                    .pickadate({
                        format: 'dd/mm/yyyy',
                        formatSubmit: 'yyyy-mm-dd',
                        selectMonths: true,
                        selectYears: true,
                        selectYears: 200,
                        hiddenName: true,
                        hiddenPrefix: '',
                        hiddenSuffix: ''
                    });
            }
        };

        _initInnerZoomImg = function() {
            if ($imgZoom.length) {
                $imgZoom.each(function(index, el) {
                    var $el = $(el),
                        widthEl = $el.width();
                    $el.imagesLoaded(function() {
                        var $img = $el.children('img'),
                            widthImg = $img.width();
                        $el.addClass('full-img');

                        if (widthImg > widthEl) {
                            $img.data('zoom-image', $img.attr('src'));
                            $img.elevateZoom({
                                zoomType: "inner",
                                cursor: "crosshair",
                                zoomWindowFadeIn: 500,
                                zoomWindowFadeOut: 750
                            });
                        }
                    });
                });
            }
        };

        // developer-1: fix for picker-frame
        _setPos4PickerFrame = function() {
            var width = $win.width(),
                height = $win.height();
            $('.picker__frame').css({
                'top': height / 5 + 'px'
            });
        };

        // // developer-1: limit extension when select file to upload
        // $('input[type="file"]').each(function() {
        //     var arr = [ "png", "jpg", "jpeg", "gif", "pdf", "doc", "docx", "xls", "xlsx", "zip", "csv", "txt" ];
        //     $(this).attr('accept', '.' + arr.join(',.'));
        //     $(this).off().on('change', function() {
        //         var inp = document.getElementById('upload_file');
        //         if (inp && inp.files.length > 1) {
        //             for (var i = 0; i < inp.files.length; ++i) {
        //                 var myfile = inp.files.item(i).name;
        //                 if (myfile) {
        //                     var ext = myfile.split('.').pop();
        //                     if (arr.indexOf(ext.toLowerCase()) < 0) {
        //                         alert(ext + ' geen toestemming!');
        //                         $(this).replaceWith($(this).val('').clone(true));
        //                         delete inp.files[i];
        //                         $('div#cate_shv_document').hide();
        //                         return false;
        //                     }
        //                     else {
        //                         $('div#cate_shv_document').show();
        //                         can_upload = true;
        //                     }
        //                 }
        //             }
        //             return;
        //         }
        //         else {
        //             var myfile = $(this).val();
        //             if (myfile) {
        //                 var ext = myfile.split('.').pop();
        //                 if (arr.indexOf(ext.toLowerCase()) < 0) {
        //                     alert(ext + ' geen toestemming!');
        //                     $(this).replaceWith($(this).val('').clone(true));
        //                     $('div#cate_shv_document').hide();
        //                     return false;
        //                 }
        //                 else {
        //                     $('div#cate_shv_document').show();
        //                     can_upload = true;
        //                 }
        //             }
        //         }
        //     });
        // });

        _bindEvent4UploadFile = function() {
            // if ($('input[type="file"]').length <= 0)
            //     return;

            // console.log("*_bindEvent4UploadFile");
            // console.log($('input[type="file"]'));

            // developer-1: limit extension when select file to upload
            $('input[type="file"]').each(function() {
                // console.log($(this));

                var arr = ["png", "jpg", "jpeg", "gif", "pdf", "doc", "docx", "odt", "xls", "xlsx", "zip", "csv", "txt", "msg"];
                $(this).attr("accept", "." + arr.join(",."));
                $(this).off().on("change", function() {
                    var inp = document.getElementById("upload_file");
                    if (inp && inp.files.length > 1) {
                        for (var i = 0; i < inp.files.length; ++i) {
                            var file = inp.files.item(i).name;
                            if (file) {
                                var ext = file.split(".").pop();
                                if (arr.indexOf(ext.toLowerCase()) < 0) {
                                    alert(ext + " geen toestemming!");
                                    $(this).replaceWith($(this).val("").clone(true));
                                    delete inp.files[i];
                                    return false;
                                }
                            }
                        }
                        return;
                    } else {
                        var file = $(this).val();
                        if (file) {
                            var ext = file.split(".").pop();
                            if (arr.indexOf(ext.toLowerCase()) < 0) {
                                alert(ext + " geen toestemming!");
                                $(this).replaceWith($(this).val("").clone(true));
                                return false;
                            }
                        }
                    }

                    // Show selected file name
                    if (!inp) {
                        var file = $(this).val();
                        if (file)
                            $(this).parent().find("span:eq(0)").html(file.replace(/^.*[\\\/]/, ''));
                        else
                            $(this).parent().find("span:eq(0)").html("Bladeren...");
                    }
                });
            });
        };

        // var _bindEvent4UploadFile_try = 0,
        //     _bindEvent4UploadFile_time;

        // _bindEvent4UploadFile = function() {
        //     console.log(_bindEvent4UploadFile_try);

        //     if (_bindEvent4UploadFile_try < 5 && $('input[type="file"]').length <= 0) {
        //         console.log("->call to set");
        //         _bindEvent4UploadFile_time = setTimeout(__bindEvent4UploadFile(), 500);
        //         _bindEvent4UploadFile_try++;
        //     }
        //     else {
        //         console.log("->clear set");
        //         clearTimeout(_bindEvent4UploadFile_time);
        //         // _bindEvent4UploadFile_try = 0;
        //     }
        // }

        _reformatPlan = function() {
            String.prototype.replaceAll = function(search, replacement) {
                var target = this;
                return target.replace(new RegExp(search, 'g'), replacement);
            };

            if (!$('li[data-block-type="casus_plan"]').hasClass("active"))
                return;

            // developer-1: re-styles for casus-plan
            var plan_print_text = $("div#plan_print_text");
            if (plan_print_text.length) {
                // {* if $casusdata.created lt 1467738000 *}
                var tmp_plan = plan_print_text.html();

                // if (plan_print_text.length && tmp_plan.length > 0 
                //     && (plan_print_text.html().indexOf("_________________________________________________________") != -1
                //     || plan_print_text.html().indexOf("<b>") != -1)
                // ) {
                //     // Pre-format for old casus
                //     tmp_plan = tmp_plan.replaceAll("<p>", "<div>");
                //     tmp_plan = tmp_plan.replaceAll("</p>", "</div>");
                //     tmp_plan = tmp_plan.replaceAll("<b>", "<div>");
                //     tmp_plan = tmp_plan.replaceAll("</b>", "</div>");
                //     tmp_plan = tmp_plan.replaceAll("<div>__________________________________________________________</div>", "");

                //     plan_print_text.empty().html(tmp_plan);
                //     plan_print_text.find("table").removeAttr('style').removeAttr('border').removeAttr('cellpadding');
                //     plan_print_text.find("table").find('td').removeAttr('style');
                //     plan_print_text.find("table").removeClass().addClass("table table-bordered table-condensed");

                //     // Re-format for all
                //     plan_print_text.find('table').addClass('table table-bordered table-condensed');
                //     plan_print_text.children().addClass('panel panel-default');
                //     plan_print_text.children().children().each(function(){
                //         if ($(this).is("div"))
                //             $(this).addClass('panel-heading');
                //     });
                //     plan_print_text.find('table').find("tr:first").addClass("head-fake");
                //     plan_print_text.find(".panel-heading").each(function(){
                //         var tmp_h3 = $(this).html();
                //         $(this).empty().html('<h3 class="panel-title">' + tmp_h3 + "</h3>")
                //     });

                //     plan_print_text.find("div.panel-default").each(function(i) {
                //         $(this).addClass("div-" + (i + 1));
                //         $(this).css({"margin-bottom":"0","border-bottom":"none","border-bottom-right-radius":"0","border-bottom-left-radius":"0"});
                //     });
                //     plan_print_text.find("table.table-bordered").each(function(i) {
                //         $(this).addClass("table-" + (i + 1));
                //         $(this).removeClass("panel-default");
                //         $(this).find("td").css({"border-top":"none"});
                //     });
                // } else {
                //     // Re-format for new
                //     plan_print_text.find('table').addClass('table table-bordered table-condensed');
                //     plan_print_text.children().addClass('panel panel-default');
                //     plan_print_text.children().children().each(function(){
                //         if ($(this).is("div"))
                //             $(this).addClass('panel-heading');
                //     });
                //     plan_print_text.find('table').find("tr:first").addClass("head-fake");
                //     plan_print_text.find(".panel-heading").each(function(){
                //         var tmp_h3 = $(this).html();
                //         $(this).empty().html('<h3 class="panel-title">' + tmp_h3 + "</h3>")
                //     });
                // }

                if (plan_print_text.length) {
                    // plan_print_text.find('table').removeAttr('style').removeAttr('border').removeAttr('cellpadding');
                    plan_print_text.find('table').addClass('table table-bordered table-condensed');
                }

                clearTimeout(timeout_reformatPlan);
            }
        };

        // // developer-1: bind datepicker for input.date
        // if ($.fn.datepicker) {
        //     $('input.date').each(function() {
        //         if ($(this).attr("name") == 'birthday') {
        //             $(this).datepicker({dateFormat: "dd/mm/yy", yearRange: "1910:+10", defaultDate: "01/01/1980", changeMonth: true, changeYear: true});
        //         }   
        //         else {
        //             $(this).datepicker({dateFormat: "dd/mm/yy", yearRange: "1910:+10", changeMonth: true, changeYear: true});
        //         }
        //     });
        // }

        // var width = $win.width();
        // if (width > 1600) {
        //     if ($(".pickodate").length) {
        //         $(".pickodate").pickadate({
        //             selectMonths: true,
        //             selectYears: 100,
        //             format: 'dd/mm/yyyy',
        //             formatSubmit: "dd/mm/yyyy",
        //             hiddenSuffix: ''
        //         });
        //     }

        //     if ($(".datepicker[name=birthday]").length) {
        //         $(".datepicker[name=birthday]").pickadate({
        //             selectMonths: true,
        //             selectYears: 100,
        //             format: 'dd/mm/yyyy',
        //             formatSubmit: "dd/mm/yyyy",
        //             max: new Date(),
        //             hiddenSuffix: ''
        //         });
        //     }
        // }
        // else {
        //     // 
        // }

        // developer-1: bind event for rows clickable
        _bindEvent4ClickableRows = function() {
            $("tr.clickable, div.clickable").each(function() {
                var attr = $(this).attr('defaultlocation');
                if (typeof attr !== typeof undefined && attr !== false) {
                    $(this).css({
                        "cursor": "pointer"
                    }).on("click", function() {
                        window.location = $(this).attr("defaultlocation");
                    });
                }
            });
        };

        // $("form").each(function(){
        //     $(this).off().on("submit", function(event) {
        //         console.log(tinyMCE);
        //         if (typeof(tinyMCE) !== typeof undefined) {
        //             // event.preventDefault();
        //             console.log(event);

        //             $("textarea.richtext").each(function() {
        //                 var content = tinyMCE.get($(this).attr("id")).getContent();
        //                 console.log(content);
        //                 // console.log($(content));
        //                 // $(content) = $(content).find("td").css({"padding":"5px"});
        //                 // tinyMCE.get($(this).attr("id")).setContent($(content));
        //                 // console.log($(content));
        //             });

        //             return false;
        //         }
        //     });
        // });

        _formatDateControl = function() {
            $('input[type="date"]').each(function() {
                var date = $(this);
                if (date.length) {
                    var val = date.val();
                    if (val) {
                        var val_tmp = val.replace(new RegExp('/', 'g'), '');
                        var isnum = /^\d+$/.test(val_tmp);
                        if (!isnum)
                            date.val().attr('placeholder', val);
                    }
                }
            });
        };

        _navigateTab = function() {
            var url = document.location.toString();
            if (url.match('#')) {
                var hash = url.split('#')[1];
                first_hash = hash.split(',')[0];
                // $('.panel-group a[href="#' + first_hash + '"]').tab('show');
                $('.panel-group').find('.collapse.in').removeClass('in');
                $('.panel-group a[href="#' + first_hash + '"]').attr('aria-expanded', true);
                $('.panel-group div[id="' + first_hash + '"]').attr('aria-expanded', true).addClass('in');
            }

            // Change hash for page-reload
            $('.panel-group a').on('shown.bs.tab', function(e) {
                window.location.hash = e.target.hash;
            })
        };

        _navright_box = function() {
            if ($(".navright_box").length) {
                $(".navright_box .arrow i").off().on('click', function() {
                    // console.log('->fire click on navright_box');
                    $(".navright_box .arrow i, .navright_box .content").toggleClass('hide');
                });
            }
        };

        // developer-1: #771, add function to check mobile
        if (typeof(window.mobileAndTabletcheck) === typeof undefined) {
            window.mobileAndTabletcheck = function() {
                var check = false;
                (function(a) {
                    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
                })(navigator.userAgent || navigator.vendor || window.opera);
                return check;
            };
            window.mobilecheck = function() {
                var check = false;
                (function(a) {
                    if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
                })(navigator.userAgent || navigator.vendor || window.opera);
                return check;
            };
        }

        // developer-1: #771, substring for title on small size
        var titleFirst = $('a.logo>span').text();
        _fixTitle = function() {
            var doc_width = $win.width();
            if (doc_width <= 620) $('a.logo>span').text($('a.logo>span').text().substring(0, 10) + '...');
            else $('a.logo>span').text(titleFirst);

            // var count=0;
            // var total=$('.header div.pull-right .btn').length;
            // $('.header div.pull-right .btn').each(function() {
            //     if ($(this).hasClass('hidden')) count++;
            // });

            var icon_width = $('.header div.pull-right').width();
            if (doc_width - icon_width < 300 || doc_width <= 375) $('a.logo').hide();
            else $('a.logo').show();
        };

        // developer-1: #771, bind event for click on right menu
        var manual_click = false;
        // var isbindEvent4ClickRightMenu = false;
        _bindEvent4ClickRightMenu = function() {
            // console.log('*_bindEvent4ClickRightMenu ');

            // console.log(isbindEvent4ClickRightMenu);
            // if (isbindEvent4ClickRightMenu)
            //     return;

            $('#casus_rmenu ul.list-navigation li.tab').each(function(i) {
                // bind event click for main right-menu
                if ($(this).hasClass('tab-2nd')) {
                    $(this).off().on("click", function(evt, redirect, manual) {
                        // console.log("*fire click on tab-2nd " + $(this).data("hash"));
                        // console.log("->redirect is " + redirect);
                        // console.log("->manual is " + manual);

                        if (typeof redirect === typeof undefined || redirect == null) redirect = true;
                        if (typeof manual === typeof undefined || manual == null) manual = true;

                        // console.log('->then redirect ' + redirect);
                        // console.log("->then manual is " + manual);

                        // scroll to
                        if (manual) 
                            $('html,body').animate({scrollTop: $('div.tabholder-2').offset().top - 100}, 'slow');

                        if (!$(this).hasClass("active") || manual) {
                            if ($(this).data("block-type")) {
                                var width = $win.width();
                                var tmp_parameters = $(this).data("block-parameters");
                                if ((typeof(window.mobileAndTabletcheck) !== typeof undefined && mobileAndTabletcheck()) || width <= 768) {
                                    // console.log("->add parameters when width is " + width + " or this is mobile device");
                                    if ($(this).data("block-parameters") !== typeof undefined && $(this).data("block-parameters") != '')
                                        tmp_parameters += ',4mb=1';
                                    else
                                        tmp_parameters += '4mb=1';
                                }
                                var updateBlockList = [];
                                updateBlockList.push({
                                    "type": $(this).data("block-type"),
                                    "id": $(this).data("block-ref"),
                                    // "parameters": $(this).data("block-parameters")
                                    "parameters": tmp_parameters
                                });
                                keepalive.blocks(updateBlockList);
                            }

                            if ($(this).hasClass("active")) {
                                // console.log('->updated block then leave');
                                return;
                            }

                            // select tab
                            $(this).closest("ul.list-navigation").find("li.tab").each(function() {
                                if ($(this).hasClass('tab-2nd')) $(this).removeClass("active");
                            });
                            $(this).removeClass('hidden').addClass("active");

                            // select tab on main-window
                            $("div.tabholder-2 ul.tabs").find("li.tab").each(function() {
                                $(this).removeClass("active").addClass("hidden");
                            });
                            $("div.tabholder-2 ul.tabs li.tab[data-hash=" + $(this).data("hash") + "]").removeClass("hidden").addClass("active");

                            // show content
                            var id = $(this).data("block-ref");
                            $("div.tabholder-2").find("div.tab-content div.tab").each(function() {
                                if ($(this).data("block-id") == id) {
                                    $(this).removeClass("hidden").addClass('active');
                                    $(this).children("div").each(function() {
                                        if ($(this).data("jqplot")) {
                                            for (i in $(this).data("jqplot")) {
                                                $(this).data("jqplot")[i].replot();
                                            }
                                        }
                                    });
                                } else {
                                    $(this).removeClass('active');
                                }
                            });

                            if (redirect) {
                                var tmp = window.location.hash.replace("#", "");
                                var hash = window.location.hash.replace("#", "").split(",");

                                // console.log('->current hash');
                                // console.log(hash);

                                if (!tmp || !hash || hash.length <= 1 || (hash.constructor === Array && hash.indexOf($(this).data("hash")) == -1)) {
                                    // update hash
                                    var hash = [];
                                    $("#casus_rmenu ul.list-navigation li.active").each(function() {
                                        if ($(this).data("hash") && hash.indexOf($(this).data("hash")) == -1) {
                                            hash.push($(this).data("hash"));
                                        }
                                    });
                                    $("div.tabholder ul.tabs li.active").each(function() {
                                        if ($(this).data("hash") && hash.indexOf($(this).data("hash")) == -1) {
                                            hash.push($(this).data("hash"));
                                        }
                                    });
                                }

                                // console.log('->hash after');
                                // console.log(hash);

                                if (hash.length > 0) {
                                    window.location.hash = hash.join(",");
                                } else {
                                    window.location.hash = '';
                                }
                            }

                            // window.last_tab = $(this).data("hash");
                            // console.log('->reset last selected tab ' + window.last_tab);

                            // console.log('->clicked on tab-2nd!');
                            return;
                        }

                        // isbindEvent4ClickRightMenu = true;

                        // console.log('->do nothing!');
                        return;
                    });
                    return;
                
                // bind event click for main right-menu
                } else {
                    $(this).off().on("click", function(evt, redirect, manual) {
                        // console.log("*fire click on " + $(this).data("hash"));
                        // console.log("->redirect is " + redirect);
                        // console.log("->manual is " + manual);

                        if (typeof redirect === typeof undefined || redirect == null) redirect = true;
                        if (typeof manual === typeof undefined || manual == null) manual = true;
                        if (manual) window.manual_click = manual;

                        // console.log('->then redirect ' + redirect);
                        // console.log("->then manual is " + manual);
                        
                        // _toggleBubbles(true);

                        // scroll to
                        if (manual) 
                            $('html,body').animate({scrollTop: $('body').offset().top - 100}, 'slow');

                        if (!$(this).hasClass("active") || manual) {
                            if ($(this).data("block-type")) {
                                var width = $win.width();
                                var tmp_parameters = $(this).data("block-parameters");
                                if ((typeof(window.mobileAndTabletcheck) !== typeof undefined && mobileAndTabletcheck()) || width <= 768) {
                                    // console.log("->add parameters when width is " + width + " or this is mobile device");
                                    if ($(this).data("block-parameters") !== typeof undefined && $(this).data("block-parameters") != '')
                                        tmp_parameters += ',4mb=1';
                                    else
                                        tmp_parameters += '4mb=1';
                                }
                                
                                var updateBlockList = [];
                                updateBlockList.push({
                                    "type": $(this).data("block-type"),
                                    "id": $(this).data("block-ref"),
                                    // "parameters": $(this).data("block-parameters")
                                    "parameters": tmp_parameters
                                });
                                keepalive.blocks(updateBlockList);
                                updateBlockList = [];

                                if (manual) {
                                    var arr = ['chome', 'message'];
                                    var menu_hash = $(this).data('hash');
                                    // console.log(arr.indexOf(menu_hash));
                                    if (menu_hash != '' && arr.indexOf(menu_hash) == -1 && typeof(reloadNotificationCenter) !== typeof undefined) {
                                        // console.log('->refresh NC');
                                        reloadNotificationCenter();
                                    }

                                    var right_menu = $("div[data-block-id=casus_rmenu]");
                                    if (menu_hash != '' && arr.indexOf(menu_hash) == -1 && right_menu.length > 0) {
                                        // console.log('->refresh right-menu');
                                        updateBlockList.push({
                                            "type": $(right_menu).data("block-type"),
                                            "id": $(right_menu).data("block-id"),
                                            "parameters": $(right_menu).data("block-parameters") + ",call_firstLoadRightMenu=false"
                                        });
                                        keepalive.blocks(updateBlockList);
                                        updateBlockList = [];
                                    }
                                }
                            }

                            if ($(this).hasClass("active")) {
                                // console.log('->updated block then leave');
                                return;
                            }

                            // select tab
                            $(this).closest("ul.list-navigation").find("li.tab").each(function() {
                                $(this).removeClass("active");
                            });
                            $(this).removeClass('hidden').addClass("active");

                            // select tab on main-window
                            $("div.tabholder-1 ul.tabs").find("li.tab").each(function() {
                                $(this).removeClass("active").addClass("hidden");
                            });
                            $("div.tabholder-1 ul.tabs li.tab[data-hash=" + $(this).data("hash") + "]").removeClass("hidden").addClass("active");

                            // show content
                            var id = $(this).data("block-ref");
                            $("div.tabholder-1").find("div.tab-content div.tab").each(function() {
                                if ($(this).data("block-id") == id) {
                                    $(this).removeClass("hidden").addClass('active');
                                    $(this).children("div").each(function() {
                                        if ($(this).data("jqplot")) {
                                            for (i in $(this).data("jqplot")) {
                                                $(this).data("jqplot")[i].replot();
                                            }
                                        }
                                    });
                                } else {
                                    $(this).removeClass('active');
                                }
                            });

                            if (redirect) {
                                var tmp = window.location.hash.replace("#", "");
                                var hash = window.location.hash.replace("#", "").split(",");

                                // console.log('->current hash');
                                // console.log(hash);

                                if (!tmp || !hash || hash.length <= 1 || (hash.constructor === Array && hash.indexOf($(this).data("hash")) == -1)) {
                                    // update hash
                                    var hash = [];
                                    $("#casus_rmenu ul.list-navigation li.active").each(function() {
                                        if ($(this).data("hash") && hash.indexOf($(this).data("hash")) == -1) {
                                            hash.push($(this).data("hash"));
                                        }
                                    });
                                    $("div.tabholder ul.tabs li.active").each(function() {
                                        if ($(this).data("hash") && hash.indexOf($(this).data("hash")) == -1) {
                                            hash.push($(this).data("hash"));
                                        }
                                    });
                                }

                                // console.log('->hash after');
                                // console.log(hash);

                                if (hash.length > 0) {
                                    window.location.hash = hash.join(",");
                                } else {
                                    window.location.hash = '';
                                }
                            }

                            window.last_tab = $(this).data("hash");
                            // console.log('->reset last selected tab ' + window.last_tab);

                            // console.log('->clicked!');
                            return;
                        }

                        // isbindEvent4ClickRightMenu = true;

                        // console.log('->do nothing!');
                        return;
                    });
                }
            });
        };

        // developer-1: #771, bind event for click on item
        var isClickHashRightMenu = 0;
        _clickHashRightMenu = function() {
            // console.log("*_clickHashRightMenu ");

            // console.log(isClickHashRightMenu);
            // if (isClickHashRightMenu)
            //     return;

            var hash = window.location.hash.replace("#", "").split(",");
            // console.log(hash);

            if (hash.constructor === Array && hash.length > 0) {
                for (var i = 0; i < hash.length; i++) {
                    if (hash[i]) {
                        // console.log("->#casus_rmenu ul.list-navigation li.tab[data-hash=" + hash[i] + "]");
                        $("#casus_rmenu ul.list-navigation li.tab[data-hash=" + hash[i] + "]").each(function() {
                            if ((!$(this).hasClass("active") || $.inArray("resize", hash))) {
                                // console.log("->click on " + hash[i]);
                                // $(this).trigger("click", [false, false]);

                                // selected menu
                                $("#casus_rmenu ul.list-navigation").find("li.tab").each(function() {
                                    $(this).removeClass("active");
                                });
                                $(this).addClass("active");

                                // if (hash[i] == 'message') hash[i]+='4mb';
                                // console.log("->also click on " + hash[i]);
                                // $("#casus_rmenu ul.list-navigation li.tab[data-hash=" + hash[i] + "]").addClass("active");

                                isClickHashRightMenu++;
                            }
                        });
                        break; //click only 1 times
                    }
                }

                // try to active tab-2nd, below-tab and below-tab-content
                if (hash.constructor === Array && hash.length > 1) {
                    $('div.inner-content').find('hr').removeClass('hidden');
                    var tab2 = hash[1];
                    var tmp = $("div.tabholder-2 ul.tabs li[data-hash=" + tab2 + "]");
                    $("div.tabholder-2 ul.tabs li[data-hash=" + tab2 + "]").removeClass('hidden');
                    $(tmp).closest('div.tabholder-2').find('div.tab-content').removeClass('hidden');
                    $("#casus_rmenu ul.list-navigation li.tab-2nd[data-hash=" + tab2 + "]").addClass('active');
                }
            }
        };

        // developer-1: #771, click on right-menu
        _bindEventOnRightMenu = function() {
            // console.log('*_bindEventOnRightMenu ');
            _bindEvent4ClickRightMenu();
            _clickHashRightMenu();
        };

        // developer-1: #771, toggle mobile tabs
        _toggleMobileTabs = function(show) {
            // console.log('*_toggleMobileTabs as ' + show + ' ');

            $("div.tabholder").find("div.tab-content div.tab").each(function(i) {
                if ($(this).hasClass("tab4mb")) {
                    if (show) $(this).toggleClass("active");
                    else $(this).removeClass("active");
                }
            });

            if (show) {
                // 
            }
            else {
                // 
            }
        };

        // developer-1: #771, toggle normal tabs
        _toggleNormalTabs = function(show) {
            // console.log('*_toggleNormalTabs as ' + show + ' ');

            // $("div.tabholder").find("ul.tabs li.tab").each(function(i) {
            //     $(this).toggleClass("active");
            // });

            // $("div.tabholder").find("div.tab-content div.tab").each(function(i) {
            //     if (!$(this).hasClass("tab4mb")) {
            //         if (show) $(this).toggleClass("active");
            //         else $(this).removeClass("active");
            //     }
            // });

            if (show) {
                // show all elements in div.inner-content
                $('div.inner-content').find('div.tabholder').removeClass('hidden');
                $('div.inner-content').find('div.tabholder li.tab').each(function() {
                    if (!$(this).hasClass("tab4mb")) $(this).removeClass('hidden');
                });
                $('div.inner-content').find('div.tabholder-2 div.tab-content').removeClass('hidden');
                $('div.inner-content').find('div.tab-content div.tab').each(function() {
                    if (!$(this).hasClass("tab4mb")) $(this).removeClass('hidden');
                });
                // $('div.inner-content').find('div[data-block-type="user_online"]').removeClass('hidden');
                $('div.inner-content').find('hr').removeClass('hidden');
            } else {
                // hide all elements in div.inner-content
                $('div.inner-content').find('div.tabholder').removeClass('hidden'); //except this
                $('div.inner-content').find('div.tabholder li.tab').each(function() {
                    if (!$(this).hasClass("tab4mb")) $(this).addClass('hidden');
                });
                $('div.inner-content').find('div.tabholder-1 div.tab-content div.tab').each(function() {
                    if (!$(this).hasClass("tab4mb")) $(this).removeClass('active').addClass('hidden');
                });
                $('div.inner-content').find('div.tabholder-2 div.tab-content').removeClass('active').addClass('hidden');
                // $('div.inner-content').find('div[data-block-type="user_online"]').addClass('hidden');
                $('div.inner-content').find('hr').each(function () {
                    if (!jQuery(this).hasClass("dr-hr")) {
                        $('div.inner-content').find('hr').addClass('hidden');
                    }
                });
            }
        };

        // developer-1: #771, select first normal tab
        _selectFirstNormalTab = function() {
            // console.log('*_selectFirstNormalTab ');

            var tmp = window.location.hash.replace("#", "");
            var hash = window.location.hash.replace("#", "").split(",");
            // console.log('->current hash');
            // console.log(hash);

            var tmp_last_tab = window.last_tab || null;
            // console.log('->last selected tab is ' + tmp_last_tab);

            // console.log('->check condition to select first normal tab');
            if (!tmp || !hash || hash.length <= 1 || (hash.constructor === Array && hash.indexOf('chome') != -1)) {
                // $("div.tabholder-1").find("ul.tabs li.tab").eq(0).addClass("active");
                // var checkedFirstTab = false;
                // $("div.tabholder-1").find("div.tab-content div.tab").each(function(i) {
                //     if (!$(this).hasClass("tab4mb") && !checkedFirstTab) {
                //         console.log('->selected tab ' + $(this).attr('id'));
                //         $(this).addClass("active").show();
                //         checkedFirstTab = true;
                //     }
                // });

                if (tmp_last_tab && $("div.tabholder-1 ul.tabs li.tab[data-hash=" + tmp_last_tab + "]").length > 0) {
                    // console.log('->trigger last select tab ' + tmp_last_tab);
                    $("div.tabholder-1 ul.tabs li.tab[data-hash=" + tmp_last_tab + "]").trigger('click', [true, false]);
                } else {
                    // console.log('->call selectFirstNormalTab');
                    selectFirstNormalTab();
                }
                _toggleMobileTabs(false);
            }
            else {
                // console.log('->else check current hash');
                if (tmp_last_tab && $("div.tabholder-1 ul.tabs li.tab[data-hash=" + tmp_last_tab + "]").length > 0) {
                    // console.log('->trigger last select tab ' + tmp_last_tab);
                    $("div.tabholder-1 ul.tabs li.tab[data-hash=" + tmp_last_tab + "]").trigger('click', [false, false]);
                }
            }
        };

        // developer-1: #771, check current hash
        _checkCurrentHash = function(right_menu) {
            // console.log('*_checkCurrentHash ');
            if (typeof right_menu === typeof undefined || right_menu == null)
                right_menu = true;
            if (right_menu)
                _clickHashRightMenu();
            else
                clickHash();
        };

        // developer-1: #771, first load right-menu
        _firstLoadRightMenu = function(manual) {
            // console.log('*_firstLoadRightMenu with manual is ' + manual);
            if (typeof manual === typeof undefined || manual == null) manual = true;
            // console.log("->then manual is " + manual);

            var tmp = window.location.hash.replace("#", "");
            var hash = window.location.hash.replace("#", "").split(",");
            // console.log('->current hash');
            // console.log(hash);

            // console.log('->show the top-right-icon for right-menu');
            $('.btn-toggle-nav-right').removeClass('hidden');

            var arr = ['plan', 'chome'];
            var tmp_last_tab = window.last_tab || null;

            // console.log('->last selected tab is ' + tmp_last_tab);
            // console.log('->last clicked by manual-click ' + window.manual_click);
            // console.log('->default selected first tab ' + window.set_default_tab);
            // console.log("->check in list ['plan', 'chome']: " + arr.indexOf(tmp_last_tab));

            // try to active tab-2nd, below-tab and below-tab-content
            if (hash.constructor === Array && hash.length > 1) {
                $('div.inner-content').find('hr').removeClass('hidden');
                var tab2 = hash[1];
                var tmp = $("div.tabholder-2 ul.tabs li[data-hash=" + tab2 + "]");
                $("div.tabholder-2 ul.tabs li[data-hash=" + tab2 + "]").removeClass('hidden');
                $(tmp).closest('div.tabholder-2').find('div.tab-content').removeClass('hidden');
                $("#casus_rmenu ul.list-navigation li.tab[data-hash=" + tab2 + "]").addClass('active');
            }

            if (window.manual_click == true && tmp_last_tab == 'plan') {
                // console.log('->last tab is plan and manual-click, do nothing');
                $("div.tabholder-1 ul.tabs li[data-hash=" + tmp_last_tab + "]").removeClass('hidden').addClass('active');
                var id = $("div.tabholder-1 ul.tabs li[data-hash=" + tmp_last_tab + "]").data('block-ref');
                $("div.tabholder-1 div.tab-content div[data-block-id=" + id + "]").removeClass('hidden').addClass('active');
                return;
            }

            // (!window.set_default_tab) || 
            // (typeof(window.set_default_tab) !== typeof undefined && window.set_default_tab == true) || 
            // && (typeof(window.set_default_tab) !== typeof undefined && window.set_default_tab == true))

            // console.log(tmp_last_tab && (arr.indexOf(tmp_last_tab) != -1 && (typeof(window.set_default_tab) !== typeof undefined && window.set_default_tab == true)) && isClickHashRightMenu);
            // console.log(tmp_last_tab && arr.indexOf(tmp_last_tab) == -1 && hash.indexOf(tmp_last_tab) != -1);

            // click on last tab when default tab is not selected
            if (tmp_last_tab == 'plan' && typeof(window.set_default_tab) == 'undefined') {
                // console.log('->select plan tab when not set default tab and it init');
                $("div.tabholder-1 ul.tabs li[data-hash=" + tmp_last_tab + "]").removeClass('hidden').addClass('active');
                var id = $("div.tabholder-1 ul.tabs li[data-hash=" + tmp_last_tab + "]").data('block-ref');
                $("div.tabholder-1 div.tab-content div[data-block-id=" + id + "]").removeClass('hidden').addClass('active');

                // _toggleMobileTabs(false);
                return;
            }

            // click on first right-menu after clicked all
            if ((tmp_last_tab && arr.indexOf(tmp_last_tab) != -1 && isClickHashRightMenu) || (tmp_last_tab && typeof(window.set_default_tab) !== typeof undefined && window.set_default_tab == true && !window.manual_click)) {
                var tab_chome = $("#casus_rmenu ul.list-navigation li.tab[data-hash=chome]");
                var tab_chome_content = $("div.tabholder-1 div.tab-content div[data-block-id=casus_home]");
                if (!tab_chome.hasClass('active') || tab_chome_content.hasClass('hidden') || !tab_chome_content.hasClass('active')) {
                    // console.log('->click on right-menu home');
                    // _selectFirstNormalTab();
                    $("#casus_rmenu ul.list-navigation li.tab[data-hash=chome]").trigger("click", [true, false]);
                    $("div.tabholder-1 div.tab-content div[data-block-id=casus_home]").removeClass('hidden');
                    // _toggleNormalTabs(false);
                } else {
                    // console.log('->click on right-menu home, already!');
                }
                return;
            }

            // show last selected tab
            if (tmp_last_tab && arr.indexOf(tmp_last_tab) == -1 && hash.indexOf(tmp_last_tab) != -1) {
                // if (tmp_last_tab == 'message') {
                //     // console.log('->trigger click for tab message');
                //     // $("#casus_rmenu ul.list-navigation li.tab[data-hash=message]").trigger('click', [false, true]);

                //     // // console.log('->reset normal-click by false');
                //     // // window.manual_click = false;

                //     console.log('->do nothing for tab ' + tmp_last_tab);
                //     return;
                // }
                
                // console.log('->show last tab ' + tmp_last_tab);
                if (manual) {
                    // console.log('->click on right-menu');
                    $("#casus_rmenu ul.list-navigation li.tab[data-hash=" + tmp_last_tab + "]").trigger("click", [false, false]);
                }
                $("div.tabholder-1 ul.tabs li[data-hash=" + tmp_last_tab + "]").removeClass('hidden').addClass('active');
                var id = $("div.tabholder-1 ul.tabs li[data-hash=" + tmp_last_tab + "]").data('block-ref');
                $("div.tabholder-1 div.tab-content div[data-block-id=" + id + "]").removeClass('hidden').addClass('active');

                // _toggleMobileTabs(false);
                return;
            }

            // no choise, select first normal tab
            // console.log('->no choise, select first normal tab');
            _selectFirstNormalTab();
            return;
        };

        // developer-1: #771, toggle bubbles
        _toggleBubbles = function(show) {
            // console.log('*_toggleBubbles as ' + show + ' ');

            // get current width
            var width = $win.width();
            if (mobileAndTabletcheck() || width <= 768)
                show = true;
            else
                show = false;
            // console.log('*_toggleBubbles-2 as ' + show + ' ');

            if (show) {
                $('span.sign4mb').removeClass('hidden');
                $('a.sign4mb').removeClass('hidden');
                $('div.hide4mb').addClass('hidden');
                $('div.hide4pc').removeClass('hidden');
            }
            else {
                $('span.sign4mb').addClass('hidden');
                $('a.sign4mb').addClass('hidden');
                $('div.hide4mb').removeClass('hidden');
                $('div.hide4pc').addClass('hidden');
            }

            _bindEvent4Bubble();
        };

        // developer-1: #771, bind event for bubbles
        _bindEvent4Bubble = function() {
            // console.log('*_bindEvent4Bubble ')
            $('span.glyphicon.sign4mb').each(function(){
                $(this).off().on('click', function() {
                    $('div.alert-' + $(this).data('bubble')).removeClass('hidden');
                    return false;
                });
            });
        };

        // developer-1: #771, substring for title on small size
        var width = $win.width();
        var is_bindEventOnRightMenu = 0;
        bindEventOnRightMenu = function(options) {
            // console.log('*bindEventOnRightMenu ');
            if (typeof options === typeof undefined) options = {};
            // console.log(options);
            // console.log('->call_firstLoadRightMenu is ' + options.call_firstLoadRightMenu);
            if (typeof options.call_firstLoadRightMenu === typeof undefined || options.call_firstLoadRightMenu == 'true')
                options.call_firstLoadRightMenu = true;
            if (options.call_firstLoadRightMenu == 'false')
                options.call_firstLoadRightMenu = false;
            // console.log('->then call_firstLoadRightMenu is ' + options.call_firstLoadRightMenu);

            // set number this function called
            is_bindEventOnRightMenu++;

            // get current width
            width = $win.width();

            // render on small/mobile size
            if (mobileAndTabletcheck() || width <= 768) {
                // console.warn("render on mobile device/small size (" + width + ")");
                $('.btn-toggle-nav-right').removeClass('hidden');
                $('.sidebar-right').removeClass('hidden');

                // // toggle navright_box
                // $(".navright_box .arrow i").each(function() {
                //     if ($(this).hasClass('fa-angle-right')) {
                //         return $(this).trigger('click');
                //     }
                // });

                _toggleNormalTabs(false);
                _toggleBubbles(true);
                _bindEventOnRightMenu();
                // if (options.call_firstLoadRightMenu == true) {
                    _firstLoadRightMenu(options.call_firstLoadRightMenu);
                // }
                _bindEvent4Bubble();
                // _startTime();

                if ($('.btn-toggle-nav-right').length > 0 && !$('.btn-toggle-nav-right').hasClass('hidden')) {
                    var hash_tmp = $('#casus_rmenu ul.menuholder-1 li.active').data('hash');
                    if (hash_tmp && hash_tmp == 'message') {
                        // console.log('->found right-menu is active ' + hash_tmp);
                        $('#casus_rmenu ul.menuholder-1 li.active').trigger('click', [false, true]);
                        return;
                    }
                }
            
            // render on big/normal size
            } else {
                // console.warn("render on big/normal size (" + width + ")");
                $('.btn-toggle-nav-right').addClass('hidden');
                $('.sidebar-right').addClass('hidden');

                // // toggle navright_box
                // $(".navright_box .arrow i").each(function() {
                //     if ($(this).hasClass('fa-angle-left')) {
                //         return $(this).trigger('click');
                //     }
                // });

                _toggleNormalTabs(true);
                _selectFirstNormalTab();

                if ($('.btn-toggle-nav-right').length > 0 && !$('.btn-toggle-nav-right').hasClass('hidden')) {
                    _toggleBubbles(false);
                }
                
                // console.log('->reset normal-click by false');
                // window.manual_click = false;

                if ($('.btn-toggle-nav-right').length > 0 && $('.btn-toggle-nav-right').hasClass('hidden')) {
                    var hash_tmp = $('div.tabholder-1 ul.tabs li.active').data('hash');
                    if (hash_tmp && hash_tmp == 'message') {
                        // console.log('->found main-tap is active ' + hash_tmp);
                        $('div.tabholder-1 ul.tabs li.active').trigger('click', [false, true]);
                        return;
                    }
                }
            }
        };

        // developer-1: #771, show timer on right-menu
        _startTime = function() {
            if ($('#time').length <= 0)
                return;

            var today = new Date();
            var hour = today.getHours();
            var minute = today.getMinutes();
            var second = today.getSeconds();
            var day = today.getDate();
            var month = today.getMonth() + 1;
            var year = today.getFullYear();
            // var ampm = hour >= 12 ? 'PM' : 'AM';
            // hour = hour % 12;
            // hour = hour ? hour : 12; // the hour '0' should be '12'
            ampm = '';
            hour = hour < 10 ? '0' + hour : hour;
            minute = minute < 10 ? '0' + minute : minute;
            second = second < 10 ? '0' + second : second;
            day = day < 10 ? '0' + day : day;
            month = month < 10 ? '0' + month : month;
            var strTime = hour + ':' + minute + ':' + second + ' ' + ampm + '' + day + '/' + month + '/' + year;
            $('#time').text(strTime);

            t = setTimeout(function() {
                _startTime();
            }, 1000);
        }; // _startTime();

        // allowZoom = function(flag) {
        //     if (flag == true) {
        //         $('head meta[name=viewport]').remove();
        //         $('head').prepend('<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=10.0, minimum-scale=1, user-scalable=1" />');
        //     } else {
        //         $('head meta[name=viewport]').remove();
        //         $('head').prepend('<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=0" />');              
        //     }
        // }; allowZoom(false);

        // _bindClickOnBodyBefore = function() {
        //     $('body:before').off().on('click', function() {
        //         alert('body:before clicked');
        //     });
        // }; _bindClickOnBodyBefore();

        // developer-1: #771, bind events for window
        $(window).on('load', function() {
            // console.log("*window.load ");
            _setPos4PickerFrame();
            _navigateTab();
            _navright_box();
            _fixTitle();

            // render on small/mobile size
            if (mobileAndTabletcheck() || width <= 768) {
                // if ($('.btn-toggle-nav-right').length > 0) {
                //     bindEventOnRightMenu();
                // }
            
            // render on big/normal size
            } else {
                // toggle navright_box
                $(".navright_box .arrow i").each(function() {
                    if ($(this).hasClass('fa-angle-right')) {
                        return $(this).trigger('click');
                    }
                });
            }
        }).on('resize', function() {
            // console.log("*window.resize ");
            _fixTitle();

            // render on small/mobile size
            if (mobileAndTabletcheck() || width <= 768) {
                _toggleBubbles(true);
            
            // render on big/normal size
            } else {
                _toggleBubbles(false);
            }

            if ($win.width() != width) {
                width = $win.width();
                // console.log('->resize width');
                // console.log(width);

                _setPos4PickerFrame();
                _initDateControl();
                _fixTitle();

                if ($('.btn-toggle-nav-right').length > 0) {
                    // console.log('->call function bindEventOnRightMenu at ' + is_bindEventOnRightMenu);
                    bindEventOnRightMenu();
                }
            }
        }).on("hashchange", function() {
            // console.log("*window.hashchange ");
            if ($('.btn-toggle-nav-right').length > 0) {
                _clickHashRightMenu();
            }
        });

    // developer-1: #771, catch any error then bind only toggle menu
    } catch (e) {
        console.warn(e);
        return {
            init: function() {
                _bindEventToggleNave();
            }
        }
    }

    return {
        init: function() {
            _bindEventToggleNave();
            _initTooltips();
            _initCollapseSwitchControl();
            _initSort();
            _initMasonryNotification();
            _initStickyElement();
            _initSelect2();
            _initDateControl();
            // _formatDateControl();
            _initInnerZoomImg();
            _bindEvent4UploadFile();
            _bindEvent4ClickableRows();
            // _navright_box();
            // _startTime();
        },

        event4Blocks: function() {
            // developer-1: limit extension when select file to upload
            _bindEvent4UploadFile();

            // developer-1: re-format tab plan
            timeout_reformatPlan = setTimeout(function() {
                _reformatPlan();
            }, 200);

            // developer-1: re-call tooltip
            _initTooltips();

            // developer-1: re-style column with action buttons
            $(document).find("a[data-original-title]").each(function() {
                var column = $(this).parent().parent();
                if ($(column).is("td")) $(column).css({
                    "width": "5%"
                });
            });

            // $(".table-responsive").find("td").each(function(i) {
            //     if (!$.trim($(this).html()).length) {
            //         $(this).hide();
            //     }
            // });

            _bindEvent4ClickableRows();
        },

        // developer-1: create open dialog
        openPopoup: function(title, content, okText, cancelText, okCallback, cancelCallback) {
            title = title || "Confirm", okText = okText || null, cancelText = cancelText || null, okCallback = okCallback || null, cancelCallback = cancelCallback || null;
            if (typeof(content) !== 'object') {
                $('body').find('div.dialog-message').remove();
                $('body').append('<div class="dialog-message hidden"></div>');
                $('body').find('div.dialog-message').html(content);
                content = $('body').find('div.dialog-message');
            }
            content = content || $('div.dialog-message');
            var options = {
                resizable: false,
                draggable: false,
                closeOnEscape: false,
                open: function(event, ui) {
                    $(this).parents('.ui-dialog-buttonpane button:eq(1)').focus();
                    $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                    // $(".ui-dialog", ui.dialog | ui).css("position", "fixed");
                    $(document).keypress(function(e) {
                        if (e.keyCode == 27) {
                            $(this).parent().find("button:eq(1)").trigger("click");
                        }
                        if (e.keyCode == 13) {
                            $(this).parent().find("button:eq(0)").trigger("click");
                        }
                    });
                },
                // height: auto,
                modal: true,
                title: title,
                buttons: [
                    // {text: okText,click: function() {$(this).dialog("close");okCallback();}},{text: cancelText,click: function() {$(this).dialog("close");cancelCallback();}}
                ]
            };
            if (okText) {
                var okButton = {
                    text: okText,
                    click: function() {
                        $(this).dialog("close");
                        if (okCallback) okCallback();
                    }
                };
                options.buttons.push(okButton);
            }
            if (cancelText) {
                var cancelButton = {
                    text: cancelText,
                    click: function() {
                        $(this).dialog("close");
                        if (cancelCallback) cancelCallback();
                    }
                };
                options.buttons.push(cancelButton);
            }
            // $(content).removeClass("hidden").dialog(options);
        }
    };
}(jQuery, window));

jQuery(document).ready(function() {
    Project.init();
});