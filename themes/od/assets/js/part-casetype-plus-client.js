function myTrim(x) {
    return x.replace(/^\s+|\s+$/gm,'');
}

jQuery(document).ready(function() {
    var requestList = [];
    var casetype_plus_process_url = casetype_plus_process_url || "?location=casetype_plus_process";
    
    var callAjax = (function(){
        var ajaxObj = $.ajax({
            type: "POST",
            url: casetype_plus_process_url,
            processData: false,
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            data: JSON.stringify({
                "blocks": requestList
            }),
            async: !1,
            error: function(jqXHR, textStatus, errorThrown) {},
            success: function(data) {}
        });

        var ajaxResponse = ajaxObj.responseText;
        
        // ajaxObj.success(function(response) {
        //     return response;
        // });

        return jQuery.parseJSON(ajaxResponse);
    });

    $('select').select2();

    var bindEvent4SelectFunding = (function() {
        if (myTrim($('select#funding_id').val()) <= 0)
            return;
        requestList.push({
            "type": "filter",
            "id": "typeofcase_id",
            "parameters": "municipality_id=" + myTrim($('select#municipality_id').val()) + ",workkind_id=" + myTrim($('select#workkind_id').val()) + ",funding_id=" + myTrim($('select#funding_id').val()) + ",typeofcase_id=" + myTrim($('#typeofcase_id_selected').val())
        });
        if (requestList.length > 0) {
            res = callAjax();
            requestList = [];
            if (res.output[0].html) {
                var parent = $('select#typeofcase_id').parent();
                $('select#typeofcase_id').select2('destroy').remove();
                $(parent).prepend(res.output[0].html).find("select").select2();
            }
        }
    });

    var bindEvent4SelectFundingChange = (function() {
        if ($('select#funding_id').length <= 0)
            return false;
        bindEvent4SelectFunding();

        $('select#funding_id').on("change", function() {
            $('select#typeofcase_id').select2('destroy').find("option").each(function() {
                if ($(this).attr("value") > 0) $(this).remove();
            });
            $('select#typeofcase_id').select2();
            bindEvent4SelectFunding();
        });
    });

    var bindEvent4SelectWorkkind = (function() {
        if (myTrim($('select#workkind_id').val()) == "")
            return;
        requestList.push({
            "type": "filter",
            "id": "funding_id",
            "parameters": "municipality_id=" + myTrim($('select#municipality_id').val()) + ",workkind_id=" + myTrim($('select#workkind_id').val()) + ",funding_id=" + myTrim($('#funding_id_selected').val())
        });
        if (requestList.length > 0) {
            res = callAjax();
            requestList = [];
            if (res.output[0].html) {
                var parent = $('select#funding_id').parent();
                $('select#funding_id').select2('destroy').remove();
                $(parent).prepend(res.output[0].html).find("select").select2();
                bindEvent4SelectFundingChange();
            }
        }
    });

    var bindEvent4SelectWorkkindChange = (function() {
        if ($('select#workkind_id').length <= 0)
            return false;
        bindEvent4SelectWorkkind();

        $('select#workkind_id').on("change", function() {
            $('select#typeofcase_id').select2('destroy').find("option").each(function() {
                if ($(this).attr("value") > 0) $(this).remove();
            });
            $('select#typeofcase_id').select2();

            $('select#funding_id').select2('destroy').find("option").each(function() {
                if ($(this).attr("value") > 0) $(this).remove();
            });
            $('select#funding_id').select2();
            bindEvent4SelectWorkkind();
        });
    });

    var bindEvent4SelectMunicipality = (function() {
        if (myTrim($('select#municipality_id').val()) == "")
            return;
        requestList.push({
            "type": "filter",
            "id": "workkind_id",
            "parameters": "municipality_id=" + myTrim($('select#municipality_id').val()) + ",workkind_id=" + myTrim($('#workkind_id_selected').val())
        });
        if (requestList.length > 0) {
            res = callAjax();
            requestList = [];
            if (res.output[0].html) {
                var parent = $('select#workkind_id').parent();
                $('select#workkind_id').select2('destroy').remove();
                $(parent).prepend(res.output[0].html).find("select").select2();
                bindEvent4SelectWorkkindChange();
            }
        }
    });

    var bindEvent4SelectMunicipalityChange = (function() {
        if ($('select#municipality_id').length <= 0)
            return false;
        bindEvent4SelectMunicipality();

        $('select#municipality_id').on("change", function() {
            $('select#typeofcase_id').select2('destroy').find("option").each(function() {
                if ($(this).attr("value") > 0) $(this).remove();
            });
            $('select#typeofcase_id').select2();
            $('select#funding_id').select2('destroy').find("option").each(function() {
                if ($(this).attr("value") > 0) $(this).remove();
            });
            $('select#funding_id').select2();
            
            $('select#workkind_id').select2('destroy').find("option").each(function() {
                if ($(this).attr("value") > 0) $(this).remove();
            });
            $('select#workkind_id').select2();
            bindEvent4SelectMunicipality();
        });
    });
    bindEvent4SelectMunicipalityChange();
});