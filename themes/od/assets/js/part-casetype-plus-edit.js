jQuery(document).ready(function() {
    var requestList = [];
    var casetype_plus_process_url = casetype_plus_process_url || "?location=casetype_plus_process";
    
    var callAjax = (function(){
        var ajaxObj = $.ajax({
            type: "POST",
            url: casetype_plus_process_url,
            processData: false,
            contentType: "application/json;charset=utf-8",
            dataType: "json",
            data: JSON.stringify({
                "blocks": requestList
            }),
            async: !1,
            error: function(jqXHR, textStatus, errorThrown) {},
            success: function(data) {}
        });

        var ajaxResponse = ajaxObj.responseText;
        
        // ajaxObj.success(function(response) {
        //     return response;
        // });

        return jQuery.parseJSON(ajaxResponse);
    });

    $('select.form-control').select2();

    var bindEvent4ModalSumit = (function(){
        // console.log('bindEvent4ModalSumit');
        $('#modal-container').find('[data-toggle="submit"]').each(function(){
            $(this).on("click", function(){
                // console.log($('#modal-container').find('.modal-content'));
                console.log($('#modal-container').find('#frm-municipality-add'));
                console.log($('#modal-container').find('#frm-municipality-add').serialize());
            });
        });
    });
    var bindEvent4ModalResult = (function(obj){
        var target = $(obj).data('target');
        requestList.push({
            "type": "modal",
            "id": target,
            "parameters": ""
        });
        if (requestList.length > 0) {
            res = callAjax();
            requestList = [];
            if (!res || !res.output[0] || !res.output[0].html) return;
            var options = {};
            $('#modal-container').html(res.output[0].html).modal(options);
            $('#modal-container').find('[data-toggle="tooltip"]').tooltip();
            bindEventForModal();
            // bindEvent4ModalSumit();
        }
        return false;
    });
    var bindEventForModal = (function(){
        $('#modal-container').find('[data-toggle="modal"]').each(function(){
            $(this).on("click", function(){
                bindEvent4ModalResult($(this));
                bindEvent4ModalSumit();
            });
        });
        $('#modal-container').find('[data-dismiss="modal"]').each(function(){
            $(this).on("click", function(){
                bindEvent4ModalResult($(this));
                // bindEvent4ModalSumit();
            });
        });
        return false;
    });

    // Step 2
    $('[data-toggle="modal"]').each(function(){
        $(this).on("click", function(){
            var target = $(this).data('target');
            requestList.push({
                "type": "modal",
                "id": target,
                "parameters": ""
            });

            if (requestList.length > 0) {
                res = callAjax();
                requestList = [];

                var options = {};
                $('#modal-container').html(res.output[0].html).modal(options);
                $('#modal-container').find('[data-toggle="tooltip"]').tooltip();

                bindEventForModal();
                
                // $('#modal-container').find('[data-toggle="modal"]').each(function(){
                //     $(this).on("click", function(){
                //         var target_child = $(this).data('target');
                //         requestList.push({
                //             "type": "modal",
                //             "id": target_child,
                //             "parameters": ""
                //         });
                //         if (requestList.length > 0) {
                //             res = callAjax();
                //             requestList = [];
                //             var options = {};
                //             $('#modal-container').html(res.output[0].html).modal(options);
                //             $('#modal-container').find('[data-dismiss="modal"]').each(function(){
                //                 $(this).on("click", function(){
                //                     var target_parent = $(this).data('target');
                //                     requestList.push({
                //                         "type": "modal",
                //                         "id": target_parent,
                //                         "parameters": ""
                //                     });
                //                     if (requestList.length > 0) {
                //                         res = callAjax();
                //                         requestList = [];
                //                         var options = {};
                //                         $('#modal-container').html(res.output[0].html).modal(options);

                //                         bindEventForModal();
                //                         return false;
                //                     }
                //                 });
                //             });
                //             return false;
                //         }
                //     });
                // });
            }
            return false;
        });
    });

    // var bindEvent4SelectFunding = (function() {
    //     if (parseInt($('select#funding_id').val()) <= 0)
    //         return;
    //     requestList.push({
    //         "type": "select",
    //         "id": "typeofcase_id",
    //         "parameters": "funding_id=" + parseInt($('select#funding_id').val()) + ",typeofcase_id=" + parseInt($('#typeofcase_id_selected').val())
    //     });
    //     if (requestList.length > 0) {
    //         res = callAjax();
    //         if (res.output[0].html) {
    //             requestList = [];
    //             var parent = $('select#typeofcase_id').parent();
    //             $('select#typeofcase_id').select2('destroy').remove();
    //             $(parent).prepend(res.output[0].html).find("select").select2();
    //         }
    //     }
    // });

    // var bindEvent4SelectFundingChange = (function() {
    //     if ($('select#funding_id').length <= 0)
    //         return false;
    //     bindEvent4SelectFunding();

    //     $('select#funding_id').on("change", function() {
    //         $('select#typeofcase_id').select2('destroy').find("option").each(function() {
    //             if ($(this).attr("value") > 0) $(this).remove();
    //         });
    //         $('select#typeofcase_id').select2();
    //         bindEvent4SelectFunding();
    //     });
    // });

    // var bindEvent4SelectWorkkind = (function() {
    //     if (parseInt($('select#workkind_id').val()) <= 0)
    //         return;
    //     requestList.push({
    //         "type": "select",
    //         "id": "funding_id",
    //         "parameters": "workkind_id=" + parseInt($('select#workkind_id').val()) + ",funding_id=" + parseInt($('#funding_id_selected').val())
    //     });
    //     if (requestList.length > 0) {
    //         res = callAjax();
    //         if (res.output[0].html) {
    //             requestList = [];
    //             var parent = $('select#funding_id').parent();
    //             $('select#funding_id').select2('destroy').remove();
    //             $(parent).prepend(res.output[0].html).find("select").select2();
    //             bindEvent4SelectFundingChange();
    //         }
    //     }
    // });

    // var bindEvent4SelectWorkkindChange = (function() {
    //     if ($('select#workkind_id').length <= 0)
    //         return false;
    //     bindEvent4SelectWorkkind();

    //     $('select#workkind_id').on("change", function() {
    //         $('select#typeofcase_id').select2('destroy').find("option").each(function() {
    //             if ($(this).attr("value") > 0) $(this).remove();
    //         });
    //         $('select#typeofcase_id').select2();

    //         $('select#funding_id').select2('destroy').find("option").each(function() {
    //             if ($(this).attr("value") > 0) $(this).remove();
    //         });
    //         $('select#funding_id').select2();
    //         bindEvent4SelectWorkkind();
    //     });
    // });

    // var bindEvent4SelectMunicipality = (function() {
    //     if (parseInt($('select#municipality_id').val()) <= 0)
    //         return;
    //     requestList.push({
    //         "type": "select",
    //         "id": "workkind_id",
    //         "parameters": "municipality_id=" + parseInt($('select#municipality_id').val()) + ",workkind_id=" + parseInt($('#workkind_id_selected').val())
    //     });
    //     if (requestList.length > 0) {
    //         res = callAjax();
    //         if (res.output[0].html) {
    //             requestList = [];
    //             var parent = $('select#workkind_id').parent();
    //             $('select#workkind_id').select2('destroy').remove();
    //             $(parent).prepend(res.output[0].html).find("select").select2();
    //             bindEvent4SelectWorkkindChange();
    //         }
    //     }
    // });

    // var bindEvent4SelectMunicipalityChange = (function() {
    //     if ($('select#municipality_id').length <= 0)
    //         return false;

    //     $('select#municipality_id').on("change", function() {
    //         $('select#typeofcase_id').select2('destroy').find("option").each(function() {
    //             if ($(this).attr("value") > 0) $(this).remove();
    //         });
    //         $('select#typeofcase_id').select2();
    //         $('select#funding_id').select2('destroy').find("option").each(function() {
    //             if ($(this).attr("value") > 0) $(this).remove();
    //         });
    //         $('select#funding_id').select2();
            
    //         $('select#workkind_id').select2('destroy').find("option").each(function() {
    //             if ($(this).attr("value") > 0) $(this).remove();
    //         });
    //         $('select#workkind_id').select2();
    //         bindEvent4SelectMunicipality();
    //     });
    // });
    // bindEvent4SelectMunicipalityChange();

    // Step 4
    var t = $('input[data-name="casusdocument"]'),
        a = $('input[data-name="referral"]'),
        e = $('input[data-name="letter"]'),
        messagedocument = $('input[data-name="messagedocument"]'),
        shv = $('input[data-name="shv"]');
    checkToDisableCasusDocument = function(n, r) {
        if (n = n || null, r = r || null, n && "referral" != $(n).attr("data-name") && "letter" != $(n).attr("data-name") && "casusdocument" != $(n).attr("data-name")) return !1;
        var c = 1 == $('input[data-name="referral"]').prop("checked"),
            i = 1 == $('input[data-name="letter"]').prop("checked");
        c || i ? (t.prop("checked", !0), t.attr("disabled", !0)) : t.attr("disabled", !1), r && "casusdocument" == $(n).attr("data-name") && 1 == $(n).prop("checked") && (a.prop("checked", !1), e.prop("checked", !1))
    }, checkToDisableCasusDocument(null), bindEventCaseTypeFeatures = function(t) {
        $(t).prop("checked") ? ("kpi" == $(t).attr("data-name") && $("#" + $(t).attr("data-name") + "_child").show(), "letter" == $(t).attr("data-name") && $("#" + $(t).attr("data-name") + "_child").show(), "contactregistratie" == $(t).attr("data-name") && $("#" + $(t).attr("data-name") + "_child").show(), "survey" == $(t).attr("data-name") && $("#" + $(t).attr("data-name") + "_child").show()) : ("kpi" == $(t).attr("data-name") && $("#" + $(t).attr("data-name") + "_child").hide(), "letter" == $(t).attr("data-name") && $("#" + $(t).attr("data-name") + "_child").hide(), "contactregistratie" == $(t).attr("data-name") && $("#" + $(t).attr("data-name") + "_child").hide(), "survey" == $(t).attr("data-name") && $("#" + $(t).attr("data-name") + "_child").hide())
    }, $("input.features").each(function() {
        bindEventCaseTypeFeatures($(this)), checkToDisableCasusDocument($(this)), $(this).on("click", function() {
            bindEventCaseTypeFeatures($(this)), checkToDisableCasusDocument($(this), "click")
        })
    }), bindEventCaseTypeFeaturesResults = function(t) {
        $(t).prop("checked") ? $("#" + $(t).attr("data-name") + "_options").show() : $("#" + $(t).attr("data-name") + "_options").hide()
    }, $("input.features-results").each(function() {
        bindEventCaseTypeFeaturesResults($(this)), $(this).on("click", function() {
            bindEventCaseTypeFeaturesResults($(this))
        })
    }), $("form").on("submit", function() {
        return $("input").each(function() {
            $(this).attr("disabled", !1)
        }), !0
    });
    
    var bindEvent4KpiFeature = (function(){
        if ($('input[data-name="kpi"]').prop("checked") != true) {
            $('.kpi-list').find('input[type="checkbox"]').each(function(){
                if ($(this).attr("disabled") != "disabled")
                    $(this).prop("checked", false);
                $('.kpi-list').find('.child-item').hide();
            });
        }
    });
    $('input[data-name="kpi"]').on("click", function(){
        bindEvent4KpiFeature();
    });
    bindEvent4KpiFeature();
    
    $('input[type="checkbox"]').each(function(){
        if ($(this).hasClass("parent-item")) {
            var parent = $(this).parent().parent();
            if ($(this).prop("checked") == true)
                parent.find('.child-item').show();
            $(this).on('click', function(){
                if ($(this).prop("checked") == true)
                    parent.find('.child-item').show();
                else
                    parent.find('.child-item').hide();
            });
        }
    });

    var bindEvent4KpiItemClick = (function(item){
        if ($(item).prop("checked") == true)
            $('input[data-name="kpi"]').prop("checked", true);
        
        $(".kpi-parent-item").change(function(){
            if ($('.kpi-parent-item:checked').length > 0)
                $('input[data-name="kpi"]').prop("checked", true);
            else
                $('input[data-name="kpi"]').prop("checked", false);
        });
    });
    bindEvent4KpiItemClick();
    $('li.kpi-item').find('input[type="checkbox"]').each(function(){
        $(this).on('click', function(){
            bindEvent4KpiItemClick($(this));
        });
    });

    var bindEvent4ContactRegistie = (function(){
        $('.custom-contactregistratie').find('input').each(function(){
            $(this).on('click', function(){
                if ($(this).prop("checked") == true) {
                    $('input[data-name="contactregistratie"]').prop("checked", true);
                }
            });
        });

        if ($('input[data-name="contactregistratie"]').prop("checked") == true) {
            var checked = 0;
            $('.custom-contactregistratie').find('input:checked').each(function(){
                if ($(this).prop("checked") == true) {
                    checked = 1;
                }
            });
            if (!checked) {
                $('.custom-contactregistratie').find('input:eq(0)').prop("checked", true);
            }
        }
    });
    $('input[data-name="contactregistratie"]').on('click', function(){
        bindEvent4ContactRegistie();
    });
    bindEvent4ContactRegistie();

    var bindEvent4Survey = (function(){
        if ($('#survey_link').val() || parseInt($('#survey_period_time').val()) > 0) {
            $('input[data-name="survey"]').prop("checked", true);
            $('.custom-survey').show();
        }
    });
    bindEvent4Survey();
    
    var bindEvent4Message = (function(){
        if ($('input[data-name="message"]').is(":checked")) {
            $('.custom-message').show();
        }
        $('input[data-name="message"]').click(function () {
            if ($('input[data-name="message"]').is(":checked")) {
                $('.custom-message').show();
            }
            else {
                $('.custom-message').hide();
            }
        });
    });
    bindEvent4Message();

    var hasCheckedForm = (function(name, id, except) {
        var flag = null;
        $(name).find('select').each(function() {
            if ($(this).attr('id') != except) {
                if ($(this).val() == id) {
                    flag = $(this);
                    return false;
                }
            }
        });
        return flag;
    });
    var bindEvent4Forms = (function(name){
        if ($(name).find('select').length <= 0)
            return;
        
        $(name).each(function(){
            $(this).find('select').on('change', function(){
                id = $(this).val();
                if (id <= 0) return;
                checked = hasCheckedForm(name, id, $(this).attr('id'));
                if (checked) $(checked).val(0).change();
                return false;
            });
        });
    });
    bindEvent4Forms('.start-form');
    bindEvent4Forms('.end-form');
    bindEvent4Forms('.aftercare-form');

    var saveChecked = (function() {
        var flag = false;
        $('.aftercare-form').find('select :selected').each(function() {
            if ($(this).val() > 0) {
                flag = true;
                return false;
            }
        });
        return flag;
    });
    var bindEvent4Aftercare = (function(){
        if ($('.aftercare-form').length <= 0)
            return;
        
        if (saveChecked())
            $('.aftercare-period-time').removeClass('hidden');
        else
            $('.aftercare-period-time').addClass('hidden').find('input').val('');
        
        $('.aftercare-form').each(function(){
            $(this).find('select').on('change', function(){
                if (saveChecked())
                    $('.aftercare-period-time').removeClass('hidden');
                else
                    $('.aftercare-period-time').addClass('hidden').find('input').val('');
            });
        });
    });
    bindEvent4Aftercare();

    var hasChecked = (function(id, except) {
        var flag = null;
        $('.codelist').find('select').each(function() {
            if ($(this).attr('id') != except) {
                if ($(this).val() == id) {
                    flag = $(this);
                    return false;
                }
            }
        });
        return flag;
    });
    var bindEvent4Codelist = (function(){
        if ($('.codelist').length <= 0)
            return;
        
        $('.codelist').each(function(){
            $(this).find('select').on('change', function(){
                id = $(this).val();
                if (id <= 0) return;
                checked = hasChecked(id, $(this).attr('id'));
                if (checked) $(checked).val(0).change();
                return false;
            });
        });
    });
    bindEvent4Codelist();
    
    $('#modal-container').on('hidden.bs.modal', function (e) {
        // console.log(e);
    });
});